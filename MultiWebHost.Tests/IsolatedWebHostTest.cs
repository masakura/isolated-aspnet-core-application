﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using MultiWebHost.WebApp;
using Xunit;

namespace MultiWebHost.Tests
{
    public sealed class IsolatedWebHostTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        public IsolatedWebHostTest(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        private readonly HttpClient _client;

        [Fact]
        public async Task 分離されたウェブアプリの設定ファイルにアクセスする()
        {
            var actual = await _client.GetStringAsync("/isolated1/configuration");

            Assert.Equal("Isolated1", actual);
        }

        [Fact]
        public async Task 分離されたウェブアプリへのリクエスト()
        {
            var actual = await _client.GetStringAsync("/isolated1");

            Assert.Equal("Hello, isolated1!", actual);
        }

        [Fact]
        public async Task 分離されたサービスにアクセスする()
        {
            var actual = await _client.GetStringAsync("/isolated1/application-name");

            Assert.Equal("isolated1", actual);
        }
    }
}