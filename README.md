﻿# 分離された ASP.NET Core アプリケーション
IdentityServer4 のようなコンポーネントは ASP.NET Core WebHost に一つしか登録できない。

このようなコンポーネントを単一の ASP.NET Core ウェブアプリケーションに登録するために、WebHost を複数同居できるようにする。