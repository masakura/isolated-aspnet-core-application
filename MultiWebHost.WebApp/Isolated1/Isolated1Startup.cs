﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace MultiWebHost.WebApp.Isolated1
{
    internal sealed class Isolated1Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new ApplicationName("isolated1"));
        }

        public void Configure(IApplicationBuilder app)
        {
            app.Map("/application-name", builder => builder.UseMiddleware<ApplicationNameMiddleware>());
            app.Map("/configuration", builder => builder.UseMiddleware<ConfigurationPageMiddleware>());
            app.UseMiddleware<Isolated1WelcomePageMiddleware>();
        }
    }
}