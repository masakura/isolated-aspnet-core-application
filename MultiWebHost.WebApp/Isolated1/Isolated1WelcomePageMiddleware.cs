﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MultiWebHost.WebApp.Isolated1
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class Isolated1WelcomePageMiddleware
    {
        private readonly ILogger<Isolated1WelcomePageMiddleware> _logger;

        // ReSharper disable once UnusedParameter.Local
        public Isolated1WelcomePageMiddleware(RequestDelegate next, ILogger<Isolated1WelcomePageMiddleware> logger)
        {
            _logger = logger;
        }

        // ReSharper disable once UnusedMember.Global
        public Task InvokeAsync(HttpContext context)
        {
            _logger.LogInformation("Hello, isolated1!");
            return context.Response.WriteAsync("Hello, isolated1!");
        }
    }
}