﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MultiWebHost.WebApp.Isolated1
{
    internal sealed class ApplicationNameMiddleware
    {
        // ReSharper disable once UnusedParameter.Local
        public ApplicationNameMiddleware(RequestDelegate next)
        {
        }

        public Task InvokeAsync(HttpContext context, ApplicationName name)
        {
            return context.Response.WriteAsync(name.ToString());
        }
    }
}