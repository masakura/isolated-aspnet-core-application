﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using MultiWebHost.Hosting;

namespace MultiWebHost.WebApp.Isolated1
{
    internal static class Isolated1ApplicationBuilderExtensions
    {
        public static void UseIsolated1(this IApplicationBuilder app)
        {
            app.UseIsolated(IsolatedWebHost.CreateDefaultBuilder("isolated1")
                .UseStartup<Isolated1Startup>());
        }
    }
}