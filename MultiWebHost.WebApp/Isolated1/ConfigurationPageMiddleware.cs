﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace MultiWebHost.WebApp.Isolated1
{
    internal sealed class ConfigurationPageMiddleware
    {
        private readonly IConfiguration _config;

        public ConfigurationPageMiddleware(RequestDelegate next, IConfiguration config)
        {
            _config = config;
        }

        public Task InvokeAsync(HttpContext context)
        {
            return context.Response.WriteAsync(_config["Name"]);
        }
    }
}