﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace MultiWebHost.Isolation
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class IsolatedWebHostMiddleware
    {
        private readonly WebHostContainer _container;

        // ReSharper disable once UnusedParameter.Local
        public IsolatedWebHostMiddleware(RequestDelegate next, IWebHostBuilder builder)
        {
            _container = new WebHostContainer(builder);
        }

        // ReSharper disable once UnusedMember.Global
        public Task InvokeAsync(HttpContext context)
        {
            return _container.Host.ProcessRequestAsync(context);
        }
    }
}