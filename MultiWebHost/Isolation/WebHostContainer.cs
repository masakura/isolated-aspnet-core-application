﻿using System;
using Microsoft.AspNetCore.Hosting;

namespace MultiWebHost
{
    internal sealed class WebHostContainer
    {
        private readonly Lazy<Host> _lazyHost;

        public WebHostContainer(IWebHostBuilder builder)
        {
            _lazyHost = new Lazy<Host>(() =>
            {
                var host = builder.Build();
                host.Start();
                return new Host(host);
            });
        }

        public Host Host => _lazyHost.Value;
    }
}