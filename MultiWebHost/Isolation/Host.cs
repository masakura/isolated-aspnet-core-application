﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MultiWebHost.Hosting.Server;

namespace MultiWebHost
{
    internal sealed class Host
    {
        private readonly IWebHost _host;

        public Host(IWebHost host)
        {
            _host = host;
        }

        public Task ProcessRequestAsync(HttpContext context)
        {
            var original = context.RequestServices;

            try
            {
                using var request =
                    new RequestServicesFeature(context, _host.Services.GetRequiredService<IServiceScopeFactory>());
                context.RequestServices = request.RequestServices;

                return _host.Services.GetRequiredService<HttpContextProxyServer>()
                    .ProcessRequestAsync(context);
            }
            finally
            {
                context.RequestServices = original;
            }
        }
    }
}