﻿using Microsoft.AspNetCore.Hosting;
using MultiWebHost.Isolation;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Builder
{
    public static class MultiWebHostApplicationBuilderExtensions
    {
        public static void UseIsolated(this IApplicationBuilder app, IWebHostBuilder isolatedWebHostBuilder)
        {
            app.UseMiddleware<IsolatedWebHostMiddleware>(isolatedWebHostBuilder);
        }
    }
}