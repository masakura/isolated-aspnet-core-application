﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MultiWebHost.Configuration
{
    internal sealed class JsonAppSettings
    {
        private readonly IConfigurationBuilder _config;
        private readonly IHostingEnvironment _env;

        public JsonAppSettings(IConfigurationBuilder config, IHostingEnvironment env)
        {
            _config = config;
            _env = env;
        }

        private string ApplicationName => _env.ApplicationName;

        private string EnvironmentName => _env.EnvironmentName;

        public JsonAppSettings AddDefault()
        {
            return AddJsonFile("appsettings.json");
        }

        public JsonAppSettings AddByEnvironment()
        {
            return AddJsonFile($"appsettings.{EnvironmentName}.json");
        }

        private JsonAppSettings AddJsonFile(string file)
        {
            _config.AddJsonFile(Path.Combine(ApplicationName, file), true, true);

            return this;
        }
    }
}