﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace MultiWebHost.Hosting.Server
{
    internal sealed class HttpContextProxyServer : IServer
    {
        private IHttpApplication _application;

        public void Dispose()
        {
        }

        public Task StartAsync<TContext>(IHttpApplication<TContext> application, CancellationToken cancellationToken)
        {
            _application = new HttpApplication<TContext>(application);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public IFeatureCollection Features { get; } = new FeatureCollection();

        public Task ProcessRequestAsync(HttpContext context)
        {
            return _application.ProcessRequestAsync(context);
        }

        private sealed class HttpApplication<TContext> : IHttpApplication
        {
            private readonly IHttpApplication<TContext> _application;

            public HttpApplication(IHttpApplication<TContext> application)
            {
                _application = application;
            }

            public Task ProcessRequestAsync(HttpContext context)
            {
                var wrapper = new HostingApplication.Context {HttpContext = context};
                return _application.ProcessRequestAsync((TContext) (object) wrapper);
            }
        }
    }
}