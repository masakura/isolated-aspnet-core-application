﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MultiWebHost.Configuration;
using MultiWebHost.Hosting.Server;

namespace MultiWebHost.Hosting
{
    public static class IsolatedWebHost
    {
        public static IWebHostBuilder CreateDefaultBuilder(string applicationName)
        {
            return new WebHostBuilder()
                .ConfigureAppConfiguration((context, config) =>
                {
                    context.HostingEnvironment.ApplicationName = applicationName;
                })
                .ConfigureAppConfiguration(ConfigureDefaultAppConfiguration)
                .ConfigureLogging(ConfigureDefaultLogging)
                .ConfigureServices(services =>
                {
                    services
                        .AddSingleton<HttpContextProxyServer>()
                        .AddSingleton<IServer>(provider => provider.GetRequiredService<HttpContextProxyServer>());
                });
        }

        private static void ConfigureDefaultAppConfiguration(WebHostBuilderContext context,
            IConfigurationBuilder config)
        {
            new JsonAppSettings(config, context.HostingEnvironment)
                .AddDefault()
                .AddByEnvironment();

            config.AddEnvironmentVariables();
        }

        private static void ConfigureDefaultLogging(WebHostBuilderContext context, ILoggingBuilder logging)
        {
            logging.AddConfiguration(context.Configuration.GetSection("Logging"));
            logging.AddConsole();
            logging.AddDebug();
        }
    }
}