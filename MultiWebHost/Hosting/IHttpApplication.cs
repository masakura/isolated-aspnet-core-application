﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MultiWebHost.Hosting
{
    internal interface IHttpApplication
    {
        Task ProcessRequestAsync(HttpContext context);
    }
}